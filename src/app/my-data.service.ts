import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class MyDataService {
  constructor(private http: Http) { }
  fetchData() {
    this.http.get('/posts').subscribe(
    )
  }
  sucess() {
    return "successful";
  }
  service1 = function (post) {
    return this.http.post('/posts', post)
      .success(function (data) {
        this.posts.push(data);
      });
  };



  service2 = function (post) {
    return this.http.put('/posts/' + post._id + '/upvote')
      .success(function (data) {
        post.upvotes += 1;
      });
  };

  factory1 = function(id) {
  return this.http.get('/posts/' + id).then(function(res){
    return res.data;
  });
};

addComment = function(id, comment) {
  return this.http.post('/posts/' + id + '/comments', comment);
};

upvoteComment = function(post, comment) {
  return this.http.put('/posts/' + post._id + '/comments/'+ comment._id + '/upvote')
    .success(function(data){
      comment.upvotes += 1;
    });
};


  factory() {
    let factory = {
      posts: []
    };
    return factory;


  }
}
