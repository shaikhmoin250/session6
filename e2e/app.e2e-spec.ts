import { PracticeangPage } from './app.po';

describe('practiceang App', function() {
  let page: PracticeangPage;

  beforeEach(() => {
    page = new PracticeangPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
