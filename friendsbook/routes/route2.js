var express = require('express');
var router = express.Router();

router.get('/posts/:post', function(req, res, next) {
  res.json(req.post);
  });

module.exports = router;
