import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule} from '@angular/router';

import { AppComponent } from './app.component';
import {MyDataService} from './my-data.service';
import {SortPipe} from './app.sort';
import { MyComponentComponent } from './my-component/my-component.component';
import { PostComponent } from './post/post.component';



@NgModule({
  declarations: [
    SortPipe,
    AppComponent,
    MyComponentComponent,
    PostComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
     {
       path:'my-component',
      component: MyComponentComponent
     },
     {
        path:'post',
      component: PostComponent
     }
    ])
  ],
  providers: [MyDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
